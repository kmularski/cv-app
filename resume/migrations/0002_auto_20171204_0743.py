# -*- coding: utf-8 -*-
# Generated by Django 1.11.7 on 2017-12-04 07:43
from __future__ import unicode_literals

import ckeditor_uploader.fields
from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('resume', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='resumeobject',
            name='description',
            field=ckeditor_uploader.fields.RichTextUploadingField(),
        ),
    ]
