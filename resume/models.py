from django.db import models
from ckeditor_uploader.fields import RichTextUploadingField


class ResumeObject(models.Model):
    header = models.CharField(max_length=100)
    description = RichTextUploadingField()

    def __str__(self):
        return self.header