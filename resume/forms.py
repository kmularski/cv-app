from crispy_forms.helper import FormHelper
from crispy_forms.layout import Submit
from django import forms
from django.urls import reverse
from django.utils.translation import ugettext_lazy as _


class ContactForm(forms.Form):
    def __init__(self, *args, **kwargs):
        super(ContactForm, self).__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.form_id = 'id-personal-data-form'
        self.helper.form_method = 'post'
        self.helper.form_action = reverse('index')
        self.helper.add_input(Submit('submit', 'Submit'))

    contact_name = forms.CharField(required=True, label='your name')
    contact_email = forms.EmailField(required=True, label='your e-mail adress')
    content = forms.CharField(
        required=True,
        widget=forms.Textarea,
        label='message'
    )


