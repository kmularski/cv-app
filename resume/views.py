from django.core.mail import EmailMessage
from django.http import Http404, HttpResponseRedirect
from django.template.loader import get_template
from django.urls import reverse
from django.utils.translation import ugettext as _
from django.views.generic.edit import FormMixin
from django.views.generic.list import ListView

from resume.forms import ContactForm
from resume.models import ResumeObject


class FormListView(FormMixin, ListView):
    def get(self, request, *args, **kwargs):
        # From ProcessFormMixin
        form_class = self.get_form_class()
        self.form = self.get_form(form_class)

        # From BaseListView
        self.object_list = self.get_queryset()
        allow_empty = self.get_allow_empty()
        if not allow_empty and len(self.object_list) == 0:
            raise Http404(_(u"Empty list and '%(class_name)s.allow_empty' is False.")
                          % {'class_name': self.__class__.__name__})

        context = self.get_context_data(object_list=self.object_list, form=self.form)
        return self.render_to_response(context)

    def post(self, request, *args, **kwargs):
        form_class = ContactForm
        if request.method == 'POST':
            form = form_class(data=request.POST)
            if form.is_valid():
                contact_name = request.POST.get(
                    'contact_name'
                    , '')
                contact_email = request.POST.get(
                    'contact_email'
                    , '')
                content = request.POST.get('content', '')

                # Email the profile with the
                # contact information
                template = get_template('contact_template.txt')
                context = {
                    'contact_name': contact_name,
                    'contact_email': contact_email,
                    'form_content': content,
                }
                content = template.render(context)

                email = EmailMessage(
                    "New message from " + context['contact_name'],
                    content,
                    "Your website" + '',
                    ['contact@karolmularski.me'],
                    headers={'Reply-To': contact_email, 'From': contact_email}
                )
                email.send()
                print('przeszło!')
                return HttpResponseRedirect(reverse('index'))


class MyListView(FormListView):
    form_class = ContactForm
    model = ResumeObject
