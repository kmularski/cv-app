from .base import *
from decouple import config

DEBUG = True

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.mysql',
        'NAME': config('DB_NAME_DEV'),
        'USER': config('DB_USER_DEV'),
        'PASSWORD': config('DB_PASSWORD_DEV'),
        'HOST': config('DB_HOST_DEV'),
        'PORT': '',
    }
}