$(document).ready(function(){

	$('a[href^="#"]').on('click',function (e) {
	    e.preventDefault();

	    var target = this.hash,
	    $target = $(target);
	    var nav = document.getElementById('top-nav');

	    $('html, body').stop().animate({
	        'scrollTop': $target.offset().top - nav.offsetHeight
	    }, 900, 'swing', function () {});
	});
});
